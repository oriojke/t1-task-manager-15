package ru.t1.didyk.taskmanager.api.controller;

public interface IProjectTaskController {

    void bindTaskToProject();

    void unbindTaskFromProject();
    
}
